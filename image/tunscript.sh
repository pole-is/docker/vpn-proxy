#!/usr/bin/env bash
set -exbm

echo "Adresse IP interne: $INTERNAL_IP4_ADDRESS" >&2
echo "Passerelle: $VPNGATEWAY" >&2

UPSTREAMS=$(echo "$CISCO_CSTP_OPTIONS" | awk -F= '/^DNS=/{print "--upstream="$2":53"}')

/bin/dnsproxy \
    --port=8053 \
    --https-port=8853 \
    --tls-crt=/etc/ssl/certs/ssl-cert-snakeoil.pem \
    --tls-key=/etc/ssl/private/ssl-cert-snakeoil.key \
    $UPSTREAMS \
    --bootstrap 1.1.1.1:53 \
    --fallback 1.1.1.1:53 \
    </dev/null >&2 &

exec /usr/bin/tunsocks -g -k 60 \
    -H 0.0.0.0:3128 \
    -D 0.0.0.0:11080 \
    -L 0.0.0.0:2222:isdevtools.irstea.fr:22 \
    -L 0.0.0.0:8080:isdevtools.irstea.fr:80 \
    -L 0.0.0.0:8843:isdevtools.irstea.fr:443