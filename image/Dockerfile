FROM ubuntu:eoan AS builder

RUN apt-get update -yq && apt-get install -yq build-essential automake libevent-dev

COPY tunsocks /src/

WORKDIR /src

RUN ./autogen.sh
RUN ./configure --prefix=/usr
RUN bash -c 'make -j $[ $(nproc) + 2 ]'
RUN mkdir /out && make install DESTDIR=/out

FROM ubuntu:eoan

RUN apt-get update -yq \
    && apt-get install -yq openconnect libevent-2.1-6 tsocks ssl-cert \
    && find /var/*/apt -type f -print -delete

COPY --from=builder /out/usr/bin/tunsocks /usr/bin/tunsocks

COPY openconnect.conf /etc/openconnect.conf
COPY docker-command.sh /bin/docker-command.sh
COPY tunscript.sh /bin/tunscript.sh
COPY tsocks.conf /etc/tsocks.conf

ARG DNSPROXY_VERSION=0.24.0
ADD https://github.com/AdguardTeam/dnsproxy/releases/download/v${DNSPROXY_VERSION}/dnsproxy-linux-amd64-v${DNSPROXY_VERSION}.tar.gz /tmp/dnsproxy.tgz
RUN cd tmp \
    && tar xfz dnsproxy.tgz \
    && mv linux-amd64/dnsproxy /bin \
    && rm -rf dnsproxy.tgz linux-amd64

RUN chmod a+rx /bin/*.sh

EXPOSE 2222/tcp 3128/tcp 8080/tcp 8443/tcp 11080/tcp

CMD ["/bin/docker-command.sh"]