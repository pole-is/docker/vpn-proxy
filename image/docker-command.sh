#!/usr/bin/env bash
set -e

VPN_OPTS=" --config=/etc/openconnect.conf"
[[ -n $VPN_USER ]] && VPN_OPTS="$VPN_OPTS --user=$VPN_USER"

if [[ -s /etc/openconnect.passwd ]]; then
    cat /etc/openconnect.passwd | exec openconnect $VPN_OPTS --passwd-on-stdin acces.intranet.inra.fr
else
    exec openconnect $VPN_OPTS acces.intranet.inra.fr
fi
