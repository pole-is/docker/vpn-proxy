#!/usr/bin/env bash
set -e

OPTS=""

if [[ -n "$1" ]]; then
    OPTS="-e VPN_USER=$1"
    shift

    if [[ -n "$1" ]]; then
        OPTS="$OPTS -v $(readlink -f $1):/etc/openconnect.passwd:ro"
        shift
    fi
fi

exec docker run --rm \
    --name=vpn-proxy \
    --init \
    --interactive --tty \
    --detach-keys "ctrl-e,ctrl-e" \
    -p "127.0.0.200:22:2222/tcp" \
    -p "127.0.0.200:53:8053/tcp" \
    -p "127.0.0.200:53:8053/udp" \
    -p "127.0.0.200:80:8080/tcp" \
    -p "127.0.0.200:443:8443/tcp" \
    -p "127.0.0.200:853:8853/tcp" \
    -p "127.0.0.200:1080:11080/tcp" \
    -p "127.0.0.200:3128:3128/tcp" \
    $OPTS \
    vpn-proxy
