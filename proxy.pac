function FindProxyForURL(url, host) {
    if (
        dnsDomainIs(host, ".inra.fr") ||
        dnsDomainIs(host, ".inrae.fr") ||
        dnsDomainIs(host, ".irstea.fr") ||
        dnsDomainIs(host, ".irstea.priv")
    ) {
        return "SOCKS5 127.0.0.200:1080";
    }
    return "DIRECT";
}
