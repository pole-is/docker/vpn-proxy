# vpn-proxy

Un proxy VPN avec et pour Docker.

## Présentation

Ce conteneur offre plusieurs services de proxy passant par le VPN de l'INRAE.

Au lieu de rerouter préemptivement les communications par le VPN, c'est vous qui
décider lesquelles devront l'être.

## Prérequis

- Docker

## Installation

```bash
git clone --recurse-submodules https://gitlab.irstea.fr/pole-is/docker/vpn-proxy.git
./build.sh
```

Si avez déjà le dépôt, vous pouvez vous contenter d'une mise-à-jour :

```bash
git pull
git submodule update --init --recursive
./build.sh
```

## Utilisation

### Démarrage

Démarrez le conteneur avec la commande suivante (après avoir modifié les droits
sur le fichier):

```bash
./start.sh
```

En cas de problème de type _Error response from daemon: driver failed
programming external connectivity on endpoint vpn-proxy_, exécuter la commande
ci-dessous :

```bash
sudo systemctl stop ssh.socket ssh.service
```

Il est mode verbeux. A un moment, votre login et mot de passe vont vous être
demandés :

```
Enter login credentials
Username: gperreal
Password: **********************
```

Quand vous arrivez à ces messages, c'est que tout est ok :

```
INFO      Apr 01 14:54:00 [7]: Finished creating all children.
INFO      Apr 01 14:54:00 [7]: Starting main loop. Accepting connections.
[2020-04-01 14:54:00] ESP session established with server
[2020-04-01 14:54:00] ESP tunnel connected; exiting HTTPS mainloop.
```

**Notes**

- Si vous interrompez le conteneur, la connexion VPN tombe avec.
- Vous pouvez toutefois "détacher" le terminal avec la séquence "Ctrl+E,Ctrl+E".
  En revanche, il faudra penser à arrêter le conteneur.

### Connexion automatique

Il est possible de configurer la connexion semi-automatique en passant deux
paramètres à start.sh:

1. votre login LDAP,
2. (optionel) le nom d'un fichier contenant votre mot de passe.

Si vous fournissez les deux, ~~j'aurai accès à votre compte~~ la connexion est
totalement automatisé et le conteneur pourra être arrêté et démarré à volonté
(avec `docker stop vpn-proxy` / `docker start vpn-proxy`).

Exemple:

```bash
# Crée un fichier .passwd en lecture limiée
touch .passwd
chmod 0600 .passwd
vim .passwd
# Lance la connexion en automatisé
./start.sh gperreal .passwd
```

### Proxies

Le conteneur expose plusieurs services de proxy sur les ports suivants:

#### Port 3128 : proxy HTTP

Ce proxy fait passer par le VPN toutes les requêtes pour inra.fr, inrae.fr,
irstea.fr et irstea.priv. Il n'est pas très performant, mais il fait le boulot.

#### Port 22 : SSH sur isdevtools.irstea.fr

Le port 22 est "mappé" sur le port 22 d'isdevtools.irstea.fr, i.e. se connecter
sur 127.0.0.2000 équivaut à se connecter à isdevtools.irstea.fr :

```
$ ssh devops@127.0.0.200
Linux isdevtools.irstea.fr 4.9.0-8-amd64 #1 SMP Debian 4.9.144-3.1 (2019-02-19) x86_64
...
```

Cela veut dire qu'on peut l'utiliser comme un "SSH proxy" pour accéder à
n'importe quel serveur:

```
$ ssh -o ProxyJump=devops@127.0.0.200 root@cin-mc-gitlab.irstea.priv
Linux cin-mc-gitlab.irstea.priv 4.19.0-5-amd64 #1 SMP Debian 4.19.37-5+deb10u2 (2019-08-08) x86_64
...
```

N.B. : il est possible de configurer SSH pour qu'il applique tout seul les
options qui vont bien (cf. fichier [ssh_config-ProxyJump](ssh_config-ProxyJump)
que vous vous copier dans ~/`.ssh.config`):

```
$ cat ~/.ssh/config
Host vpn-proxy 127.0.0.200 isdevtools.irstea.fr
    Hostname 127.0.0.200
    User devops
    ProxyJump none

Host *.irstea.fr *.irstea.priv
    ProxyJump vpn-proxy
$ ssh root@cin-mc-gitlab.irstea.priv
Linux cin-mc-gitlab.irstea.priv 4.19.0-5-amd64 #1 SMP Debian 4.19.37-5+deb10u2 (2019-08-08) x86_64
```

#### Port 80 et 443 : HTTP et HTTPS sur isdevtools.irstea.fr

Les ports 80 et 443 sont "mappés" sur les ports équivalents
d'isdevtools.irstea.fr.

Cela permet par exemple, d'utiliser la ligne suivante dans `/etc/hosts` pour
accéder à isdevtools.irstea.fr. (Attention, c'est un hack temporaire, qu'il
faudra désactiver.)

```
127.0.0.200 isdevtools.irstea.fr
```

#### Port 1080: SOCKS v5

Le SOCKS v5 permet de rediriger n'importe quelle communication TCP/IP avec tout
outil compatible. C'est le cas notamment de Curl, la majorité des navigateurs
web, etc...

#### Port 53 et 853: DNS et DNS-over-HTTPS

### proxy.pac

Le dépôt contient un fichier proxy.pac, qui peut être utilisé pour configurer
simplement les navigateurs. L'avantage est qu'il ne redirige que les
communications pour les domaines d'INRAE.

### Et avec Docker

L'idée est de brancher votre conteneur docker sur vpn-proxy.

To be continued.

## Explication

Ce conteneur utilise [openconnect](https://www.infradead.org/openconnect/) un
client VPN libre qui intègre le protocole GlobalProtect depuis sa version 8, et
disponible dans Ubutun 19.10+

Normalement, openconnect altère la configuration locale (interface réseau, table
de routage, résolution DNS, etc...) pour mettre en place le VPN. Mais dans ce
contexte il est utilisé en mode non-root avec un outil qui fournit plusieurs
protocoles de proxy différents:
[tunsocks](https://github.com/russdill/tunsocks).

En outre, un proxy DNS est inclus,
[dnsproxy](https://github.com/AdguardTeam/dnsproxy), il permet de relayer des
requêtes DNS et DNS-over-HTTP à travers le VPN. Cela peut s'avérer utile dans
certains cas.

```mermaid
graph LR
  subgraph "Poste de travail"
    ssh -- "SSH" --> tunsocks
    firefox -- "SOCKS" --> tunsocks
    firefox -- "DNS-over-HTTP" --> dnsproxy
    subgraph "Conteneur vpn-proxy"
      tunsocks ==> openconnect
      dnsproxy -- DNS --> openconnect
    end
    subgraph "Autre conteneur"
      composer -- "HTTP / SOCKS" --> tunsocks
    end
  end
  subgraph "Réseau INRAE"
    openconnect -- "1. connexion (HTTPS)" --> portal["acces.intranet.inra.fr"]
    openconnect == "2. tunnel (SSL)" ==> gateway["pad.dctlse.inra.fr / pad.dcidf.inra.fr"]
    gateway -- SSH --> isdevtools.irstea.fr -- "SSH (ProxyJump)" --> serveurs["Serveurs internes"]
    gateway -- "HTTP(S)" --> serveurs["Serveurs internes"]
  end
```
